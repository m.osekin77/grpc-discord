package service

import (
	"context"
)

// UserService Interface for API layer
type UserService interface {
	RegisterUser(context.Context) (string, error)
	LoginUser(context.Context) (string, error)
}
