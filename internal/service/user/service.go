package user

import (
	"context"
	"log"
	"v0/internal/repository"
)

type Service struct {
	dbRepository    repository.DBRepository    // postgres repo
	cacheRepository repository.CacheRepository // memcached repo
}

// Используя интерфейс здесь, при создании приложения мы можем прокинуть любую реализацию репозитория.

func NewService(dbRepository repository.DBRepository,
	cacheRepository repository.CacheRepository,
) *Service {

	return &Service{
		dbRepository:    dbRepository,
		cacheRepository: cacheRepository,
	}
}

func (s *Service) RegisterUser(ctx context.Context) (string, error) {
	err := s.dbRepository.Create(ctx)
	if err != nil {
		return "", err
	}

	return "", nil
}

func (s *Service) LoginUser(ctx context.Context) (string, error) {
	err := s.cacheRepository.CacheData(ctx)
	if err != nil {
		log.Println("error happened", err.Error())
		return "", err
	}

	return "successfully logged in", nil
}
