package app

import (
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/reflection"
	"log/slog"
	"net"
	"os"
	"v0/pkg/user_v0"
)

type App struct {
	log             *slog.Logger
	serviceProvider *serviceProvider
	gRPCServer      *grpc.Server
}

func NewApp(
	ctx context.Context,
) (*App, error) {
	a := &App{}

	err := a.initDeps(ctx)
	if err != nil {
		return nil, err
	}

	return a, nil
}

func (a *App) Run() error {
	return a.runGRPCServer()
}

func (a *App) initDeps(ctx context.Context) error {
	inits := []func(context.Context) error{
		a.initLogger,
		a.initServiceProvider,
		a.initGRPCServer,
	}

	for _, f := range inits {
		err := f(ctx)
		if err != nil {
			return err
		}
	}

	return nil
}

func (a *App) initLogger(_ context.Context) error {
	a.log = slog.New(slog.NewTextHandler(os.Stderr, nil))

	return nil
}

func (a *App) initServiceProvider(_ context.Context) error {
	a.serviceProvider = newServiceProvider()

	return nil
}

func (a *App) initGRPCServer(_ context.Context) error {
	a.gRPCServer = grpc.NewServer(grpc.Creds(insecure.NewCredentials()))

	reflection.Register(a.gRPCServer)

	user_v0.RegisterAuthServer(a.gRPCServer, a.serviceProvider.UserAPI())

	return nil
}

func (a *App) runGRPCServer() error {
	a.log.Info("server is running on", slog.String("addr", a.serviceProvider.config.GRPC.Port))
	addr := net.JoinHostPort(a.serviceProvider.config.GRPC.Host, a.serviceProvider.config.GRPC.Port)
	l, err := net.Listen("tcp", addr)

	if err != nil {
		return err
	}

	err = a.gRPCServer.Serve(l)
	if err != nil {
		return err
	}

	return nil
}
