package app

import (
	"log/slog"
	"os"
	"v0/internal/api/auth"
	"v0/internal/config"
	"v0/internal/repository"
	"v0/internal/repository/my_cache"
	"v0/internal/repository/postgres"
	"v0/internal/service"
	userService "v0/internal/service/user"
)

type serviceProvider struct {
	authAPI      *auth.ServerAPI
	dbRepository repository.DBRepository
	cacheRepo    repository.CacheRepository
	userService  service.UserService
	config       config.Config
	log          *slog.Logger
}

func newServiceProvider() *serviceProvider {
	return &serviceProvider{
		config: config.MustLoad(),
		log:    slog.New(slog.NewTextHandler(os.Stderr, nil)),
	}
}

func (s *serviceProvider) CreateDBRepo() repository.DBRepository {
	conn, err := postgres.Connect(s.config.Postgres)
	if err != nil {
		s.log.Info("failed to connect to postgres", slog.String("err", err.Error()))
		return nil
	}

	if s.dbRepository == nil {
		s.dbRepository = postgres.NewPostgres(conn)
	}
	s.log.Info("successfully connected to the postgres", slog.String("addr", s.config.Postgres.Port))

	return s.dbRepository
}

func (s *serviceProvider) CreateCacheRepo() repository.CacheRepository {
	client, err := my_cache.Connect(s.config.MemCached)
	if err != nil {
		s.log.Info("failed to initialize memcached")
		return nil
	}

	if s.cacheRepo == nil {
		s.cacheRepo = my_cache.NewMemCache(client)
	}

	s.log.Info("successfully created memcached client", slog.String("addr", s.config.MemCached.Port))

	return s.cacheRepo
}

func (s *serviceProvider) UserService() service.UserService {
	if s.userService == nil {
		s.userService = userService.NewService(s.CreateDBRepo(), s.CreateCacheRepo())
	}

	return s.userService
}

func (s *serviceProvider) UserAPI() *auth.ServerAPI {
	if s.authAPI == nil {
		s.authAPI = auth.NewServerAPI(s.UserService())
	}

	return s.authAPI
}
