package postgres

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5/pgxpool"
	"v0/internal/config"
)

type Postgres struct {
	conn *pgxpool.Pool
}

func NewPostgres(conn *pgxpool.Pool) *Postgres {
	return &Postgres{
		conn: conn,
	}
}

func Connect(cfg config.PostgresConfig) (*pgxpool.Pool, error) {
	url := fmt.Sprintf("postgres://%s:%s@%s:%s/%s",
		cfg.User, cfg.Password, cfg.Host, cfg.Port, cfg.DBName)

	conn, err := pgxpool.New(context.Background(), url)

	if err != nil {
		return nil, err
	}

	return conn, nil
}

func (r *Postgres) Create(ctx context.Context) error {
	return nil
}
