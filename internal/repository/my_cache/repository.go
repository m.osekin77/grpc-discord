package my_cache

import (
	"context"
	"fmt"
	"github.com/bradfitz/gomemcache/memcache"
	"log"
	"v0/internal/config"
)

type MemCache struct {
	client *memcache.Client
}

func NewMemCache(mc *memcache.Client) *MemCache {
	return &MemCache{client: mc}
}

func Connect(cfg config.MemCachedConfig) (*memcache.Client, error) {
	url := fmt.Sprintf("%v:%v", cfg.Host, cfg.Port)
	memcachedInstance := memcache.New(url)

	return memcachedInstance, nil
}

func (mc *MemCache) CacheData(ctx context.Context) error {
	mc.client.Set(&memcache.Item{Key: "foo", Value: []byte("my value")})
	it, err := mc.client.Get("foo")
	if err != nil {
		return err
	}
	log.Println("cached item", string(it.Value))

	return nil
}
