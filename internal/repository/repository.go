package repository

import "context"

// DBRepository Interface for Service Layer
type DBRepository interface {
	Create(context.Context) error
}

// CacheRepository Interface for Service Layer
type CacheRepository interface {
	CacheData(context.Context) error
}
