package auth

import (
	"context"
	"v0/pkg/user_v0"
)

// API layer

func (s *ServerAPI) Login(
	ctx context.Context,
	r *user_v0.LoginRequest,
) (*user_v0.LoginResponse, error) {
	// implement me

	resp, err := s.userService.LoginUser(ctx)

	if err != nil {
		return &user_v0.LoginResponse{Token: "bad token"}, nil
	}

	return &user_v0.LoginResponse{Token: resp}, nil
}

func (s *ServerAPI) Register(
	ctx context.Context,
	r *user_v0.RegisterRequest,
) (*user_v0.RegisterResponse, error) {
	// implement me
	return &user_v0.RegisterResponse{UserId: "12345"}, nil
}
