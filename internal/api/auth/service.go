package auth

import (
	"v0/internal/service"
	"v0/pkg/user_v0"
)

type ServerAPI struct {
	user_v0.UnimplementedAuthServer
	userService service.UserService
}

func NewServerAPI(userService service.UserService) *ServerAPI {
	return &ServerAPI{
		userService: userService,
	}
}
