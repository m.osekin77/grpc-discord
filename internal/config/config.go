package config

import (
	"github.com/ilyakaznacheev/cleanenv"
	"time"
)

type Config struct {
	GRPC      GRPCConfig      `yaml:"grpc"`
	Postgres  PostgresConfig  `yaml:"postgres"`
	MemCached MemCachedConfig `yaml:"memcached"`
}

type GRPCConfig struct {
	Host    string        `yaml:"host"`
	Port    string        `yaml:"port"`
	Timeout time.Duration `yaml:"timeout"`
}

type PostgresConfig struct {
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	User     string `yaml:"postgres"`
	Password string `yaml:"password"`
	DBName   string `yaml:"db_name"`
	Driver   string `yaml:"driver"`
}

type MemCachedConfig struct {
	Host string `yaml:"host"`
	Port string `yaml:"port"`
}

func MustLoad() Config {
	config := Config{}
	if err := cleanenv.ReadConfig("./config/config.yaml", &config); err != nil {
		panic(err)
	}

	return config
}
