Requests: grpc
Persistent DB: postgres
Cache DB: memcache
Broker: kafka

# install grpc and protobuf compiler
go get -u google.golang.org/grpc
go get -u google.golang.org/protobuf


// API LAYER -> SERVICE LAYER -> REPO LAYER -> DB LAYER
1. Create docker-compose:
    1.1 postgres [+]
    1.2 memcached [+]
    1.3 nats-streaming