package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"v0/internal/app"
)

func main() {
	ctx := context.Background()

	a, err := app.NewApp(ctx)
	if err != nil {
		log.Println("failed to initialize app", err.Error())
	}

	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, syscall.SIGTERM, syscall.SIGINT)

	go func() {
		err = a.Run()
		if err != nil {
			log.Println("failed to run app", err.Error())
		}
	}()

	<-shutdown

}
